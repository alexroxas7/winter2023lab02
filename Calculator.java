public class Calculator
{
	public static double add(double fNumber, double sNumber)
	{
		return fNumber + sNumber;
	}
	
	public static double subtract(double fNumber, double sNumber)
	{
		return fNumber - sNumber;
	}
	
	public double multiply(double fNumber, double sNumber)
	{
		return fNumber * sNumber;
	}
	
	public double divide(double fNumber, double sNumber)
	{
		return fNumber / sNumber;
	}
}