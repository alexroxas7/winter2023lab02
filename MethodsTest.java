public class MethodsTest
{
	public static void main(String[] args)
	{
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		System.out.println();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		System.out.println();
		methodTwoInputNoReturn(1, 2.5);
		System.out.println();
		int z = methodNoInputReturnInt();
		System.out.println(z);
		System.out.println();
		double summed = sumSquareRoot(9, 5);
		System.out.println(summed);
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println();
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int input)
	{
		System.out.println("Inside the method one input no return");
		input -= 5;
		System.out.println(input);
		
	}
	public static void methodTwoInputNoReturn(int woa, double woawoa)
	{
		System.out.println(woa + " " + woawoa);
	}
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	public static double sumSquareRoot(int firstInt, int secondInt)
	{
		int sum = firstInt + secondInt;
		double result = Math.sqrt(sum);
		return result;
	}
	
}